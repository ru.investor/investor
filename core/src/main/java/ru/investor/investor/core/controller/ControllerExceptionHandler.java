package ru.investor.investor.core.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import ru.investor.globalexceptionhandler.CommonExceptionHandlerControllerAdvice;
import ru.investor.globalexceptionhandler.CommonListErrorMessageHandlers;

/**
 * Обработчик ошибок.
 */
@ControllerAdvice
public class ControllerExceptionHandler extends CommonExceptionHandlerControllerAdvice {

    public ControllerExceptionHandler(CommonListErrorMessageHandlers errorMessageHandlers) {
        super(errorMessageHandlers);
    }
}
