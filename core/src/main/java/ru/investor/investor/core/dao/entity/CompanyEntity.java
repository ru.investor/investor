package ru.investor.investor.core.dao.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.investor.investor.core.dao.entity.common.DefaultSystemAttributes;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Сущность Кампания.
 */
@Entity
@Table(name = "company")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class CompanyEntity extends DefaultSystemAttributes {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "company_seq_gen")
    @SequenceGenerator(name = "company_seq_gen", sequenceName = "company_seq", allocationSize = 1)
    @Column(name = "company_id", nullable = false)
    private Long id;

    @Column(name = "company_name")
    private String name;

    @OneToMany(mappedBy = "company")
    @ToString.Exclude
    private List<WalletCompanyEntity> wallets;
}
