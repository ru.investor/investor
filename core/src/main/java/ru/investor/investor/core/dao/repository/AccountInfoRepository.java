package ru.investor.investor.core.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.investor.investor.core.dao.entity.AccountInfoEntity;

/**
 * Репозиторий для сущности Информация об аккаунте.
 */
@Repository
public interface AccountInfoRepository extends JpaRepository<AccountInfoEntity, Long> {
}
