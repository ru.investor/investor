package ru.investor.investor.core.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import ru.investor.investor.core.dao.entity.AccountInfoEntity;
import ru.investor.investor.core.dto.accountInfo.AccountInfoDto;
import ru.investor.investor.core.mapper.common.CommonMapper;

/**
 * Мапер сущности Информация об акаунте.
 */
@Mapper(componentModel = "spring", uses = WalletMapper.class)
public interface AccountInfoMapper extends CommonMapper<AccountInfoEntity, AccountInfoDto> {

    @Override
    void updateEntityFromDto(AccountInfoDto dto, @MappingTarget AccountInfoEntity entity);
}
