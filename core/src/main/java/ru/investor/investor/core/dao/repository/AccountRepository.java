package ru.investor.investor.core.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.investor.investor.core.dao.entity.AccountEntity;

/**
 * Репозиторий для сущности Акаунт.
 */
@Repository
public interface AccountRepository extends JpaRepository<AccountEntity, Long> {
}
