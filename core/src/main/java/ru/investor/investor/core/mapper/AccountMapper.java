package ru.investor.investor.core.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import ru.investor.investor.core.dao.entity.AccountEntity;
import ru.investor.investor.core.dto.account.AccountDto;
import ru.investor.investor.core.dto.account.AccountRequestDto;
import ru.investor.investor.core.mapper.common.CommonMapper;

/**
 * Мапер сущности Аккаунт.
 */
@Mapper(componentModel = "spring", uses = WalletMapper.class)
public interface AccountMapper extends CommonMapper<AccountEntity, AccountDto> {

    @Override
    void updateEntityFromDto(AccountDto dto, @MappingTarget AccountEntity entity);

    /**
     * Мапинг в сущность Акаунт.
     */
    AccountEntity toEntity(AccountRequestDto dto);
}
