package ru.investor.investor.core.mapper.common;

import java.util.List;

/**
 * Общий интерфейс для мапперов.
 *
 * @param <E> сущность
 * @param <D> дто
 */
public interface CommonMapper<E, D> {

    /**
     * Маппинг - дто -> сущность.
     *
     * @param dto {@link D}
     * @return {@link E}
     */
    E toEntity(D dto);

    /**
     * Маппинг - сущность -> дто.
     *
     * @param entity {@link E}
     * @return {@link D}
     */
    D toDto(E entity);

    /**
     * Маппинг коллекции - сущность -> дто.
     *
     * @param entity {@link E}
     * @return {@link D}
     */
    List<D> toDto(List<E> entity);

    /**
     * Обновить поля сущности полями дто.
     *
     * @param dto    дто
     * @param entity сущность
     */
    void updateEntityFromDto(D dto, E entity);
}
