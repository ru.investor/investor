package ru.investor.investor.core.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.investor.investor.core.dao.entity.RequestEntity;

/**
 * Репозиторий для сущности Запрос.
 */
@Repository
public interface RequestRepository extends JpaRepository<RequestEntity, Long> {
}
