package ru.investor.investor.core.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import ru.investor.investor.core.dao.entity.PersonEntity;
import ru.investor.investor.core.dto.person.PersonDto;
import ru.investor.investor.core.mapper.common.CommonMapper;

/**
 * Мапер сущности Пользователь.
 */
@Mapper(componentModel = "spring")
public interface PersonMapper extends CommonMapper<PersonEntity, PersonDto> {

    @Override
    void updateEntityFromDto(PersonDto dto, @MappingTarget PersonEntity entity);
}
