package ru.investor.investor.core.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.investor.investor.core.dto.person.PersonDto;

/**
 * Дто сущности Запрос.
 */
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Дто сущности Запрос")
@Data
public class RequestDto {

    @Schema(description = "Идентификатор")
    private Long id;

    @Schema(description = "Пользователь")
    private PersonDto person;

    @Schema(description = "Идентификатор кампании")
    private Long companyId;

    @Schema(description = "Количество акций")
    private Long sharesNum;

    @Schema(description = "Сумма")
    private Long price;
}
