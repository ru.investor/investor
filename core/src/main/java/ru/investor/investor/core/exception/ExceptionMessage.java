package ru.investor.investor.core.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Перечисление сообщений ошибок.
 */
@Getter
@RequiredArgsConstructor
public enum ExceptionMessage {

    ENTITY_NOT_FOUND("Не удалось найти запись по идентификатору %s");

    private final String message;
}
