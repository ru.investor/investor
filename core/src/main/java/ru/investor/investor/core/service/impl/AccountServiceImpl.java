package ru.investor.investor.core.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import ru.investor.dto.paging.InvestorPageRequest;
import ru.investor.dto.paging.InvestorPageResponse;
import ru.investor.globalexceptionhandler.dto.ExceptionCodes;
import ru.investor.investor.core.dao.entity.AccountEntity;
import ru.investor.investor.core.dao.repository.AccountRepository;
import ru.investor.investor.core.dto.account.AccountDto;
import ru.investor.investor.core.dto.account.AccountRequestDto;
import ru.investor.investor.core.exception.ExceptionMessage;
import ru.investor.investor.core.exception.InvestorBackendRuntimeException;
import ru.investor.investor.core.mapper.AccountMapper;
import ru.investor.investor.core.service.api.AccountService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Сервис для работы с сущностью Аккаунт.
 */
@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;

    @Override
    public AccountDto getById(Long id) {
        AccountEntity entity = accountRepository.findById(id).orElseThrow(
                () -> new InvestorBackendRuntimeException(ExceptionCodes.ENTITY_NOT_FOUND,
                        String.format(ExceptionMessage.ENTITY_NOT_FOUND.getMessage(), id)));
        return accountMapper.toDto(entity);
    }

    @Override
    public InvestorPageResponse<AccountDto> getAll(InvestorPageRequest request) {
        Page<AccountEntity> entities = accountRepository.findAll(request.toPageRequest());
        List<AccountEntity> content = entities.getContent();

        List<AccountDto> response = content.stream()
                .map(accountMapper::toDto)
                .collect(Collectors.toList());

        return new InvestorPageResponse<>(entities.getTotalElements(), entities.getTotalPages(), response);
    }

    @Override
    public Long addAccount(AccountRequestDto accountDto) {
        AccountEntity entity = accountMapper.toEntity(accountDto);
        AccountEntity save = accountRepository.save(entity);
        return save.getId();
    }

    @Override
    public void deleteById(Long id) {
        accountRepository.deleteById(id);
    }

    @Override
    public AccountDto updateById(AccountDto accountDto, Long id) {
        AccountEntity entity = accountMapper.toEntity(accountDto);
        entity.setId(id);
        AccountEntity save = accountRepository.save(entity);
        return accountMapper.toDto(save);
    }
}
