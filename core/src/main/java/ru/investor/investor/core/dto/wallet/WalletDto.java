package ru.investor.investor.core.dto.wallet;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.investor.investor.core.dto.CompanyDto;

import java.util.List;

/**
 * Дто сущности Кошелёк.
 */
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Дто сущности Кошелёк")
@Data
public class WalletDto {

    @Schema(description = "Идентификатор")
    private Long id;

    @Schema(description = "Список кампаний")
    private List<CompanyDto> companies;
}
