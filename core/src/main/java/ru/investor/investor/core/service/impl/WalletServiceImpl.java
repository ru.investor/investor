package ru.investor.investor.core.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import ru.investor.dto.paging.InvestorPageRequest;
import ru.investor.dto.paging.InvestorPageResponse;
import ru.investor.globalexceptionhandler.dto.ExceptionCodes;
import ru.investor.investor.core.dao.entity.WalletEntity;
import ru.investor.investor.core.dao.repository.WalletRepository;
import ru.investor.investor.core.dto.wallet.WalletDto;
import ru.investor.investor.core.exception.ExceptionMessage;
import ru.investor.investor.core.exception.InvestorBackendRuntimeException;
import ru.investor.investor.core.mapper.WalletMapper;
import ru.investor.investor.core.service.api.WalletService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Сервис для работы с сущностью Кошелёк.
 */
@Service
@RequiredArgsConstructor
public class WalletServiceImpl implements WalletService {

    private final WalletRepository walletRepository;
    private final WalletMapper walletMapper;

    @Override
    public WalletDto getById(Long id) {
        WalletEntity entity = walletRepository.findById(id)
                .orElseThrow(() -> new InvestorBackendRuntimeException(ExceptionCodes.ENTITY_NOT_FOUND,
                        String.format(ExceptionMessage.ENTITY_NOT_FOUND.getMessage(), id)));
        return walletMapper.toDto(entity);
    }

    @Override
    public InvestorPageResponse<WalletDto> getAll(InvestorPageRequest request) {
        Page<WalletEntity> entityPage = walletRepository.findAll(request.toPageRequest());
        List<WalletDto> dtos = entityPage.getContent().stream()
                .map(walletMapper::toDto)
                .collect(Collectors.toList());
        return new InvestorPageResponse<>(entityPage.getTotalElements(), entityPage.getTotalPages(), dtos);
    }
}
