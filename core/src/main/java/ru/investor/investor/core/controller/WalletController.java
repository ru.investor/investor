package ru.investor.investor.core.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.investor.dto.paging.InvestorPageRequest;
import ru.investor.dto.paging.InvestorPageResponse;
import ru.investor.investor.core.dto.account.AccountDto;
import ru.investor.investor.core.dto.wallet.WalletDto;
import ru.investor.investor.core.service.api.WalletService;

/**
 * Контроллер сущности Кошелёк.
 */
@RestController
@RequestMapping("wallet")
@RequiredArgsConstructor
@Tag(name = "WalletController", description = "Контроллер сущности Кошелёк")
public class WalletController {

    private final WalletService walletService;

    /**
     * Получить кошелёк по идентификатору.
     *
     * @param id идентификатор аккаунта
     * @return кошелёк
     */
    @GetMapping("/{id}")
    @Operation(summary = "Получить кошелёк по идентификатору")
    public WalletDto getById(@PathVariable Long id) {
        return walletService.getById(id);
    }

    /**
     * Получить постранично все записи.
     *
     * @param request параметры пагинации
     * @return {@link InvestorPageResponse} {@link AccountDto} страница с кошельком
     */
    @PostMapping("/all")
    @Operation(summary = "Получить постранично все записи")
    public InvestorPageResponse<WalletDto> getAll(@RequestBody InvestorPageRequest request) {
        return walletService.getAll(request);
    }
}
