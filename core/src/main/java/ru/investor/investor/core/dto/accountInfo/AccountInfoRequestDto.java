package ru.investor.investor.core.dto.accountInfo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.investor.investor.core.dto.wallet.WalletDto;
import ru.investor.investor.core.enums.AccountStatusEnum;
import ru.investor.investor.core.enums.AccountTypeEnum;

/**
 * Дто запроса сущности Информация об аккаунте.
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Schema(description = "Дто запроса сущности Информация об аккаунте")
public class AccountInfoRequestDto {

    @Schema(description = "Статус счёта")
    private AccountStatusEnum status;

    @Schema(description = "Тип счёта")
    private AccountTypeEnum accountType;

    @Schema(description = "Баланс счёта")
    private Long balance;

    @Schema(description = "Кошелёк")
    private WalletDto wallet;
}
