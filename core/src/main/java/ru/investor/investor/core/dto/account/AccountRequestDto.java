package ru.investor.investor.core.dto.account;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.investor.investor.core.dto.accountInfo.AccountInfoRequestDto;
import ru.investor.investor.core.dto.person.PersonRequestDto;

/**
 * Дто запроса сущности Аккаунт.
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Schema(description = "Дто запроса сущности Аккаунт")
public class AccountRequestDto {

    @Schema(description = "Пользователь")
    private PersonRequestDto person;

    @Schema(description = "Информация об аккаунте")
    private AccountInfoRequestDto accountInfo;
}
