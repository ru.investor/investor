package ru.investor.investor.core.dao.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.investor.investor.core.dao.entity.common.DefaultSystemAttributes;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.OneToMany;
import java.util.Set;

/**
 * Сущность Кошелёк.
 */
@Entity
@Table(name = "wallet")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class WalletEntity extends DefaultSystemAttributes {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wallet_seq_gen")
    @SequenceGenerator(name = "wallet_seq_gen", sequenceName = "wallet_seq", allocationSize = 1)
    @Column(name = "wallet_id", nullable = false)
    private Long id;

    @OneToMany(mappedBy = "wallet")
    @ToString.Exclude
    private Set<WalletCompanyEntity> companies;
}