package ru.investor.investor.core.dao.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.investor.investor.core.dao.entity.common.DefaultSystemAttributes;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.OneToOne;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;

/**
 * Сущность Запрос.
 */
@Entity
@Table(name = "request")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RequestEntity extends DefaultSystemAttributes {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "request_seq_gen")
    @SequenceGenerator(name = "request_seq_gen", sequenceName = "request_seq", allocationSize = 1)
    @Column(name = "request_id", nullable = false)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "person_id", nullable = false)
    @ToString.Exclude
    private PersonEntity person;

    @Column(name = "company_id", nullable = false)
    private Long companyId;

    @Column(name = "shares_num", nullable = false)
    private Long sharesNum;

    @Column(name = "price")
    private Long price;
}