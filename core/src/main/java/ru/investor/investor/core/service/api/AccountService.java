package ru.investor.investor.core.service.api;

import ru.investor.dto.paging.InvestorPageRequest;
import ru.investor.dto.paging.InvestorPageResponse;
import ru.investor.investor.core.dto.account.AccountDto;
import ru.investor.investor.core.dto.account.AccountRequestDto;

/**
 * Сервис для работы с сущностью Аккаунт.
 */
public interface AccountService {

    /**
     * Получить аккаунт по идентификатору.
     *
     * @param id идентификатор аккаунта
     * @return аккаунт
     */
    AccountDto getById(Long id);

    /**
     * Получить постранично все записи.
     *
     * @param request параметры пагинации
     * @return {@link InvestorPageResponse} {@link AccountDto} страница с аккаунтом
     */
    InvestorPageResponse<AccountDto> getAll(InvestorPageRequest request);

    /**
     * Добавить новый аккаунт.
     *
     * @param accountDto аккаунт
     * @return идентификатор аккаунта
     */
    Long addAccount(AccountRequestDto accountDto);

    /**
     * Удалить аккаунт по идентификатору.
     *
     * @param id идентификатор аккаунта
     */
    void deleteById(Long id);

    /**
     * Обновить информацию об аккаунте по идентификатору.
     *
     * @param accountDto новая информация об аккаунте
     * @param id иднетификатор аккаунта
     * @return {@link AccountDto} обновлённая информация
     */
    AccountDto updateById(AccountDto accountDto, Long id);
}
