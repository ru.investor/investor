package ru.investor.investor.core.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import ru.investor.investor.core.dao.entity.RequestEntity;
import ru.investor.investor.core.dto.RequestDto;
import ru.investor.investor.core.mapper.common.CommonMapper;

/**
 * Мапер сущности Запрос.
 */
@Mapper(componentModel = "spring")
public interface RequestMapper extends CommonMapper<RequestEntity, RequestDto> {

    @Override
    void updateEntityFromDto(RequestDto dto, @MappingTarget RequestEntity entity);
}
