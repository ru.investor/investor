package ru.investor.investor.core.dao.entity.common;

/**
 * Значения индикатора изменения строки.
 */
public enum ActionIndex {

    I, U, D
}
