package ru.investor.investor.core.service.api;

import ru.investor.dto.paging.InvestorPageRequest;
import ru.investor.dto.paging.InvestorPageResponse;
import ru.investor.investor.core.dto.account.AccountDto;
import ru.investor.investor.core.dto.wallet.WalletDto;

/**
 * Сервис для работы с сущностью Кошелёк.
 */
public interface WalletService {

    /**
     * Получить кошелёк по идентификатору.
     *
     * @param id идентификатор аккаунта
     * @return кошелёк
     */
    WalletDto getById(Long id);

    /**
     * Получить постранично все записи.
     *
     * @param request параметры пагинации
     * @return {@link InvestorPageResponse} {@link AccountDto} страница с кошельком
     */
    InvestorPageResponse<WalletDto> getAll(InvestorPageRequest request);
}
