package ru.investor.investor.core.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.DeleteMapping;
import ru.investor.dto.paging.InvestorPageRequest;
import ru.investor.dto.paging.InvestorPageResponse;
import ru.investor.investor.core.dto.account.AccountDto;
import ru.investor.investor.core.dto.account.AccountRequestDto;
import ru.investor.investor.core.service.api.AccountService;

/**
 * Контроллер для сущности Аккаунт.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/account")
@Tag(name = "AccountController", description = "Контроллер для сущности Аккаунт")
public class AccountController {

    private final AccountService accountService;

    /**
     * Получить аккаунт по идентификатору.
     *
     * @param id идентификатор аккаунта
     * @return аккаунт
     */
    @GetMapping("/{id}")
    @Operation(summary = "Получить аккаунт по идентификатору")
    public AccountDto getById(@PathVariable Long id) {
        return accountService.getById(id);
    }

    /**
     * Получить постранично все записи.
     *
     * @param request параметры пагинации
     * @return {@link InvestorPageResponse} {@link AccountDto} страница с аккаунтом
     */
    @PostMapping("/all")
    @Operation(summary = "Получить постранично все записи")
    public InvestorPageResponse<AccountDto> getAll(@RequestBody InvestorPageRequest request) {
        return accountService.getAll(request);
    }

    /**
     * Добавить новый аккаунт.
     *
     * @param accountDto аккаунт
     * @return идентификатор аккаунта
     */
    @PostMapping("/add")
    @Operation(summary = "Добавить новый аккаунт")
    public Long addAccount(@RequestBody AccountRequestDto accountDto) {
        return accountService.addAccount(accountDto);
    }

    /**
     * Удалить аккаунт по идентификатору.
     *
     * @param id идентификатор аккаунта
     */
    @DeleteMapping("/{id}")
    @Operation(summary = "Удалить аккаунт по идентификатору")
    public void deleteById(@PathVariable Long id) {
        accountService.deleteById(id);
    }

    /**
     * Обновить информацию об аккаунте по идентификатору.
     *
     * @param accountDto новая информация об аккаунте
     * @param id иднетификатор аккаунта
     * @return {@link AccountDto} обновлённая информация
     */
    @PutMapping("/update/{id}")
    @Operation(summary = "Обновить информацию об аккаунте по идентификатору")
    public AccountDto updateById(@RequestBody AccountDto accountDto, @PathVariable Long id) {
        return accountService.updateById(accountDto, id);
    }
}
