package ru.investor.investor.core.dao.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;

/**
 * Соединительная таблица Кошелёк-Кампания.
 */
@Entity
@Table(name = "wallet_company")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class WalletCompanyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wallet_company_seq_gen")
    @SequenceGenerator(name = "wallet_company_seq_gen", sequenceName = "wallet_company_seq", allocationSize = 1)
    @Column(name = "wallet_company_id", nullable = false)
    private Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "wallet_id")
    @ToString.Exclude
    private WalletEntity wallet;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "company_id")
    @ToString.Exclude
    private CompanyEntity company;

    @Column(name = "volume")
    private Long volume;
}
