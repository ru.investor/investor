package ru.investor.investor.core.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.springframework.util.CollectionUtils;
import ru.investor.investor.core.dao.entity.CompanyEntity;
import ru.investor.investor.core.dao.entity.WalletCompanyEntity;
import ru.investor.investor.core.dao.entity.WalletEntity;
import ru.investor.investor.core.dto.CompanyDto;
import ru.investor.investor.core.dto.wallet.WalletDto;
import ru.investor.investor.core.mapper.common.CommonMapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Мапер сущности Кошелёк.
 */
@Mapper(componentModel = "spring")
public interface WalletMapper extends CommonMapper<WalletEntity, WalletDto> {

    @Override
    void updateEntityFromDto(WalletDto dto, @MappingTarget WalletEntity entity);

    @Override
    default WalletDto toDto(WalletEntity entity) {
        if (entity == null) {
            return null;
        }

        WalletDto walletDto = new WalletDto();
        walletDto.setId(entity.getId());
        Set<WalletCompanyEntity> companies = entity.getCompanies();
        List<CompanyDto> companyDtos = new ArrayList<>();

        if (!CollectionUtils.isEmpty(companies)) {
            for (WalletCompanyEntity company : companies) {
                CompanyEntity companyEntity = company.getCompany();
                CompanyDto companyDto = new CompanyDto();
                companyDto.setId(companyEntity.getId());
                companyDto.setName(companyEntity.getName());
                companyDto.setVolume(company.getVolume());
                companyDtos.add(companyDto);
            }
        }

        walletDto.setCompanies(companyDtos);
        return walletDto;
    }

    @Override
    default WalletEntity toEntity(WalletDto dto) {
        if (dto == null) {
            return null;
        }

        WalletEntity walletEntity = new WalletEntity();

        walletEntity.setId(dto.getId());
        List<CompanyDto> companies = dto.getCompanies();
        Set<WalletCompanyEntity> companyEntities = new HashSet<>();

        if (!CollectionUtils.isEmpty(companies)) {
            for (CompanyDto companyDto : companies) {
                CompanyEntity companyEntity = new CompanyEntity();
                companyEntity.setId(companyDto.getId());
                companyEntity.setName(companyDto.getName());

                WalletCompanyEntity wce = new WalletCompanyEntity();
                wce.setWallet(walletEntity);
                wce.setCompany(companyEntity);
                wce.setVolume(companyDto.getVolume());

                companyEntities.add(wce);
            }
        }
        walletEntity.setCompanies(companyEntities);

        return walletEntity;
    }
}
