package ru.investor.investor.core.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.investor.investor.core.dao.entity.PersonEntity;

/**
 * Репозиторий для сущности Пользователь.
 */
@Repository
public interface PersonRepository extends JpaRepository<PersonEntity, Long> {
}
