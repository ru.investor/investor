package ru.investor.investor.core.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Статусы аккаунтов.
 */
@Getter
@RequiredArgsConstructor
public enum AccountStatusEnum {

    OK("Активный"),
    BLOCK("Заблокированый");

    private final String name;
}
