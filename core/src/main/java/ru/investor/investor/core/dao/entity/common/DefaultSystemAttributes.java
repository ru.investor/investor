package ru.investor.investor.core.dao.entity.common;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

/**
 * Общие системные атрибуты.
 */
@MappedSuperclass
@Getter
@Setter
public class DefaultSystemAttributes {

    /**
     * Дата создания.
     */
    @Column(name = "create_dttm", nullable = false, updatable = false)
    @CreatedDate
    private Date createdDate;

    /**
     * Дата изменения.
     */
    @Column(name = "modify_dttm", nullable = false)
    @LastModifiedDate
    private Date modifyDate;

    /**
     * Индикатор изменения строки.
     */
    @Column(name = "action_ind", nullable = false)
    @Enumerated(EnumType.STRING)
    private ActionIndex actionInd = ActionIndex.I;

    public Date getCreatedDate() {
        return createdDate != null ? (Date) createdDate.clone() : null;
    }

    public Date getModifyDate() {
        return modifyDate != null ? (Date) modifyDate.clone() : null;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate != null ? (Date) createdDate.clone() : null;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate != null ? (Date) modifyDate.clone() : null;
    }
}
