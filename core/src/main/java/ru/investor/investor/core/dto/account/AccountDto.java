package ru.investor.investor.core.dto.account;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.investor.investor.core.dto.accountInfo.AccountInfoDto;
import ru.investor.investor.core.dto.person.PersonDto;

/**
 * Дто сущности Аккаунт.
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Schema(description = "Дто сущности Аккаунт")
public class AccountDto {

    @Schema(description = "Идентификатор")
    private Long id;

    @Schema(description = "Пользователь")
    private PersonDto person;

    @Schema(description = "Информация об аккаунте")
    private AccountInfoDto accountInfo;
}
