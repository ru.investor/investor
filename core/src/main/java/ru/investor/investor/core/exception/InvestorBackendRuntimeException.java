package ru.investor.investor.core.exception;

import ru.investor.globalexceptionhandler.exceptions.AbstractRuntimeException;

/**
 * The type Investor backend runtime exception.
 */
public class InvestorBackendRuntimeException extends AbstractRuntimeException {

    private static final String PREFIX = "INV";

    /**
     * Instantiates a new Investor backend runtime exception.
     *
     * @param code    the code
     * @param message the message
     */
    public InvestorBackendRuntimeException(String code, String message) {
        super(PREFIX, code, message);
    }

    /**
     * Instantiates a new Investor backend runtime exception.
     *
     * @param code    the code
     * @param message the message
     * @param cause   the cause
     */
    public InvestorBackendRuntimeException(String code, String message, Throwable cause) {
        super(PREFIX, code, message, cause);
    }
}
