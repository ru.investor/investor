package ru.investor.investor.core.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Дто сущности Кампания.
 */
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Дто сущности Кампания")
@Data
public class CompanyDto {

    @Schema(description = "Идентификатор")
    private Long id;

    @Schema(description = "Название")
    private String name;

    @Schema(description = "Количество")
    private Long volume;
}
