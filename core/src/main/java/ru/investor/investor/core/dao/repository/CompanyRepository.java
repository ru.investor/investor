package ru.investor.investor.core.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.investor.investor.core.dao.entity.CompanyEntity;

/**
 * Репозиторий сущности Кампания.
 */
@Repository
public interface CompanyRepository extends JpaRepository<CompanyEntity, Long> {
}
