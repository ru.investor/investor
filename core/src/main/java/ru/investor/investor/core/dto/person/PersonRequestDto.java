package ru.investor.investor.core.dto.person;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Дто запроса сущности Пользователь.
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Schema(description = "Дто запроса сущности Пользователь")
public class PersonRequestDto {

    @Schema(description = "Идентификатор пользователя в системе")
    private Long userId;

    @Schema(description = "Фамилия")
    private String lastName;

    @Schema(description = "Имя")
    private String firstName;

    @Schema(description = "Отчество/")
    private String middleName;
}
