package ru.investor.investor.core.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Типы кошельков.
 */
@Getter
@RequiredArgsConstructor
public enum AccountTypeEnum {

    STANDARD("Брокерский счёт"),
    IIS("ИИС");

    private final String name;
}
