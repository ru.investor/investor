package ru.investor.investor.core.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.investor.investor.core.dao.entity.WalletEntity;

/**
 * Репозиторий сущности Кошелёк.
 */
@Repository
public interface WalletRepository extends JpaRepository<WalletEntity, Long> {
}
