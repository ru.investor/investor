package ru.investor.investor.core.dto.accountInfo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.investor.investor.core.dto.wallet.WalletDto;
import ru.investor.investor.core.enums.AccountStatusEnum;
import ru.investor.investor.core.enums.AccountTypeEnum;

/**
 * Дто сущности Информация об аккаунте.
 */
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Дто сущности Информация об аккаунте")
@Data
public class AccountInfoDto {

    @Schema(description = "Идентификатор")
    private Long id;

    @Schema(description = "Статус счёта")
    private AccountStatusEnum status;

    @Schema(description = "Тип счёта")
    private AccountTypeEnum accountType;

    @Schema(description = "Баланс счёта")
    private Long balance;

    @Schema(description = "Кошелёк")
    private WalletDto wallet;
}
