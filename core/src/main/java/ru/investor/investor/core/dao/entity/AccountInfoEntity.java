package ru.investor.investor.core.dao.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.investor.investor.core.dao.entity.common.DefaultSystemAttributes;
import ru.investor.investor.core.enums.AccountStatusEnum;
import ru.investor.investor.core.enums.AccountTypeEnum;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.OneToOne;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;

/**
 * Сущность Информация об аккаунте.
 */
@Entity
@Table(name = "account_info")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AccountInfoEntity extends DefaultSystemAttributes {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_info_seq_gen")
    @SequenceGenerator(name = "account_info_seq_gen", sequenceName = "account_info_seq", allocationSize = 1)
    @Column(name = "account_info_id", nullable = false)
    private Long id;

    @Column(name = "status", nullable = false, length = 5)
    @Enumerated(EnumType.STRING)
    private AccountStatusEnum status;

    @Column(name = "account_type", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private AccountTypeEnum accountType;

    @Column(name = "balance", nullable = false)
    private Long balance;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "wallet_id")
    @ToString.Exclude
    private WalletEntity wallet;
}