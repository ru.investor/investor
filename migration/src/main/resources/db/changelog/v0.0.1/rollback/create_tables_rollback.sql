drop table if exists wallet;
drop table if exists accountEntity;
drop table if exists account_info;
drop table if exists request;
drop table if exists person;

drop sequence if exists wallet_seq;
drop sequence if exists account_seq;
drop sequence if exists account_info_seq;
drop sequence if exists request_seq;
drop sequence if exists person_seq;