/* Пользователь */

create sequence if not exists person_seq start 1;

create table if not exists person (
    person_id bigint not null default nextval('person_seq'),
    user_id bigint not null default -1,
    last_name varchar(255) not null default 'N/D',
    first_name varchar(255) not null default 'N/D',
    middle_name varchar(255),
    create_dttm timestamptz  not null default now(),
    modify_dttm timestamptz,
    action_ind varchar(1) not null default 'I',
    constraint pk_person_id primary key (person_id)
    );

comment on table person is 'Пользователь';
comment on column person.person_id is 'Идентификатор пользователя';
comment on column person.user_id is 'Идентификатор пользователя в системе';
comment on column person.last_name is 'Фамилия пользователя';
comment on column person.first_name is 'Имя пользователя';
comment on column person.middle_name is 'Отчество пользователя';
comment on column person.create_dttm is 'Дата создания записи';
comment on column person.modify_dttm is 'Дата обновления записи';
comment on column person.action_ind is 'Идентификатор состояния';

/** Заявки */

create sequence if not exists request_seq start 1;

create table if not exists request (
    request_id bigint not null default nextval('request_seq'),
    person_id bigint not null default -1,
    company_id bigint not null,
    shares_num bigint not null default 0,
    price bigint,
    create_dttm timestamptz  not null default now(),
    modify_dttm timestamptz,
    action_ind varchar(1) not null default 'I',
    constraint pk_request_id primary key (request_id),
    constraint fk_request_person foreign key (person_id)
        references person (person_id)
);

comment on table request is 'Заявка';
comment on column request.request_id is 'Идентификатор заявки';
comment on column request.person_id is 'Идентификатор пользователя';
comment on column request.company_id is 'Идентификатор кампании';
comment on column request.shares_num is 'Колличество акций';
comment on column request.price is 'Сумма';
comment on column request.create_dttm is 'Дата создания записи';
comment on column request.modify_dttm is 'Дата обновления записи';
comment on column request.action_ind is 'Идентификатор состояния';

/* Кошелёк */

create sequence if not exists wallet_seq start 1;

create table if not exists wallet (
    wallet_id bigint not null default nextval('wallet_seq'),
    company_id bigint not null,
    account_id bigint not null default -1,
    create_dttm timestamptz  not null default now(),
    modify_dttm timestamptz,
    action_ind varchar(1) not null default 'I',
    constraint pk_wallet primary key (wallet_id),
    unique (wallet_id, company_id)
);

comment on table wallet is 'Кошелёк';
comment on column wallet.wallet_id is 'Идентификатор кошелька';
comment on column wallet.company_id is 'Идентификатор кампании';
comment on column wallet.account_id is 'Идентификатор аккаунта';
comment on column wallet.volume is 'Колличество акций';
comment on column wallet.create_dttm is 'Дата создания записи';
comment on column wallet.modify_dttm is 'Дата обновления записи';
comment on column wallet.action_ind is 'Идентификатор состояния';

/* Информация об аккаунте */

create sequence if not exists account_info_seq start 1;

create table if not exists account_info (
    account_info_id bigint not null default nextval('account_info_seq'),
    status varchar(5) not null default 'N/D',
    account_type varchar(20) not null default 'N/D',
    balance bigint not null default 0,
    wallet_id bigint not null default -1,
    create_dttm timestamptz  not null default now(),
    modify_dttm timestamptz,
    action_ind varchar(1) not null default 'I',
    constraint pk_account_info_id primary key (account_info_id),
    constraint fk_account_wallet foreign key (wallet_id)
        references wallet (wallet_id)
);

comment on table account_info is 'Информация об аккаунте';
comment on column account_info.account_info_id is 'Идентификатор записи';
comment on column account_info.status is 'Статус счёта';
comment on column account_info.account_type is 'Тип счёта';
comment on column account_info.balance is 'Баланс счёта';
comment on column account_info.wallet_id is 'Кошелёк счёта';
comment on column account_info.create_dttm is 'Дата создания записи';
comment on column account_info.modify_dttm is 'Дата обновления записи';
comment on column account_info.action_ind is 'Идентификатор состояния';

/* Аккаунт */

create sequence if not exists account_seq start 1;

create table if not exists account (
    account_id bigint not null default nextval('account_seq'),
    person_id bigint not null default -1,
    account_info_id bigint not null default -1,
    create_dttm timestamptz  not null default now(),
    modify_dttm timestamptz,
    action_ind varchar(1) not null default 'I',
    constraint pk_account_id primary key (account_id),
    constraint fk_account_person foreign key (person_id)
        references person (person_id),
    constraint fk_account_account_info foreign key (account_info_id)
        references account_info (account_info_id)
);

comment on table account is 'Аккаунт';
comment on column account.account_id is 'Идентификатор аккаунта';
comment on column account.person_id is 'Идентификатор пользователя';
comment on column account.account_info_id is 'Идентификатор информации об аккаунте';
comment on column account.create_dttm is 'Дата создания записи';
comment on column account.modify_dttm is 'Дата обновления записи';
comment on column account.action_ind is 'Идентификатор состояния';

alter table if exists wallet add constraint fk_wallet_account foreign key (account_id)
    references account (account_id);