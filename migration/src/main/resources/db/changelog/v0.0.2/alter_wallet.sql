alter table if exists wallet drop constraint if exists fk_wallet_account;
alter table if exists wallet drop column if exists account_id;
alter table if exists wallet drop column if exists company_id;
alter table if exists wallet drop column if exists volume;
alter table if exists wallet drop constraint if exists wallet_wallet_id_company_id_key;