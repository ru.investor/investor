drop table if exists wallet_company;
drop table if exists company;

drop sequence if exists wallet_company_seq;
drop sequence if exists company_seq;