alter table if exists wallet add column account_id bigint not null default -1;
alter table if exists wallet add constraint fk_wallet_account foreign key (account_id)
    references account (account_id);