create sequence if not exists company_seq start 1;

create table if not exists company (
    company_id bigint not null default nextval('company_seq'),
    company_name varchar(100) not null default 'N/D',
    create_dttm timestamptz  not null default now(),
    modify_dttm timestamptz,
    action_ind varchar(1) not null default 'I',
    constraint pk_company primary key (company_id)
);

comment on table company is 'Камапания';
comment on column company.company_id is 'Идентификатор кампании';
comment on column company.company_name is 'Наименование кампании';
comment on column company.create_dttm is 'Дата создания записи';
comment on column company.modify_dttm is 'Дата обновления записи';
comment on column company.action_ind is 'Идентификатор состояния';

create sequence if not exists wallet_company_seq start 1;

create table if not exists wallet_company (
    wallet_company_id bigint not null default nextval('wallet_company_seq'),
    wallet_id bigint not null,
    company_id bigint not null,
    volume bigint not null default 0,
    constraint pk_wallet_company primary key (wallet_company_id),
    constraint fk_wallet_company_company foreign key (company_id)
        references company (company_id),
    constraint fk_wallet_company_wallet foreign key (wallet_id)
        references wallet (wallet_id)
);

comment on table wallet_company is 'Связка кошелька с компаниями';
comment on column wallet_company.company_id is 'Идентификатор кампании';
comment on column wallet_company.wallet_id is 'Идентификатор кошелька';